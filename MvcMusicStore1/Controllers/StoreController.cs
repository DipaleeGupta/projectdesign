﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace MvcMusicStore1.Controllers
{
    public class StoreController : Controller
    {
       
        // sample action
        public IActionResult Sample()
        {
            ViewBag.message = "This is the viewbag property message";
            return View("Sample");
        }
        
        public string Index()
        {
            return  "Hello from store Index()";
        }
        //public string Browse()
        //{
           // return "Hello from store Browse()";
       // }
       public string Browse(string genre)
        {
            return "You're browsing for album genre: " + "<h1>" + genre + "</h1>";
        }
        //public string Details()
        //{
            //return "Hello from store Details()";
        //}
        public string Details(Int32 id)
        {
            return $"Album key provided is:{id:D5}";
        }
    }
}